1. Kas yra JRE?
  A. java runtime environment
  B. java runtime exception
  C. java runtime execution

2. Kas yra JDK?
  A. java development kit
  B. java deployment kit
  C. java demo kit

3. Ar galima išplėsti abstrakčią (abstract) klasę (realizuoti paveldėjimą)?
  A. Taip
  B. Ne
  C. Taip, jei klasė nėra final

4. Ar galima išplėsti final klasę (realizuoti paveldėjimą)?
  A. Taip
  B. Ne
  C. Taip, jei klasė nėra abstract

5. Duotos klasės:
  class Base {
    public String a = "1 ";
  }
  class Derived extends Base {
    public String a = "2";
  }

  Ką išves šis kodo fragmentas?
  {
    Base b = new Derived();
    System.out.println("a" + b.a.charAt(1));
  }
  A. 1
  B. a
  C. Iškris RuntimeException

6. Duotos klasės:
  class Base {
    public int a = 1;
  }
  class Derived extends Base {
    public int a = 2;
  }
  Ką išves šis kodo fragmentas?
  {
    Base b = new Derived();
    System.out.println(b.a);
  }
  A. 1
  B. 2
  C. Iškris RuntimeException

7. Kokia išvestis?

  int a = 10;
  String name = null;
  try {
    a = name.length();
    a++;
  }
  catch (IndexOutOfBoundsException e) {
    ++a;
  }
  System.out.println(a);

  A. 10
  B. 11
  C. Iškris RuntimeException

8. Kurios iš pateiktų Stream operacijų yra baigiamosios (terminal), po kuriu nebegalima dirbti su Stream?
  A. map
  B. forEach
  C. collect
  D. filter

9. Kokia nauda iš final klasės, jeigu neleidžiama jos išplėsti (paveldėti)?
  A. Naudą gauname tik tada, jei papildomai klasę pažymime static.
  B. Teiginys klaidingas, klasę galima išplėsti.
  C. Leidžia būti tikriems, jog elgsena aprašyta klasėje nebus pakeista.

10. Kokia išvestis ?
  class Base {
    void g() {
      System.out.print("g-Base "");
      f();
    }
    void f() {
      System.out.print("f-Base ");
      g();
    }
  }

  class Derived extends Base {
    public static void main(String[] args) {
      Base b = new Derived();
      b.g();
   }

    void g() {
      System.out.print("g-Derived ");
      super.g();
    }

    void f() {
      System.out.print("f-Derived ");
    }
  }
  A. g-Derived g-Base f-Derived
  B. Programa pakibs
  C. g-Base f-Base g-Derived

11. Kaip uždrausti kviesti metodą vienu metu iš skirtingų Gijų (Thread)?
  A. Naudoti Future klasės, kuriuos skirtos dirbti su asinchroniniais procesais.
  B. Naudoti synchronize modifikatorių.
  C. Uždrausti neįmanoma

12. Duotas kodo fragmentas, ką atspauzdins programa:
   public static void main(String[] args) {
        ExecutorService executor = Executors.newFixedThreadPool(5);

        Future<String> futureA = executor.submit(() -> {
            Thread.sleep(1000);
            System.out.print("fA ");
            return "fA";
        });
        System.out.print("1 ");

        Future<String> futureB = executor.submit(() -> {
            Thread.sleep(500);
            System.out.print("fB ");
            return "fB";
        });
        System.out.print("2 ");

        System.out.print(futureA.get() + " " + futureB.get());
   }
  A. fA 1 fB 2 fA fB
  B. fB 1 fA 2 fA fB
  C. 1 2 fB fA fA fB

13. Kuri iš pagrindinio JAVA metodo signatūra yra teisinga:
  A. public static int main(String[] args)
  B. public int main(String[] args)
  C. public static void main(String[] args)

14. Kokia yra String reikšmė pagal nutylėjimą (default)?

  String a;
  System.out.println(a);

  A. null
  B. ""
  C. ''

15. Ką atspauzdins programa?

  public class Test {
    public static void main(String[] args) {
      int j = 5;
      for (int i = 0; i< j; i++) {
         if (i <= j--)
           System.out.print(i + " * " + j + " = " + (i * j) + " ");
      }
    }
  }

  A. 0 3 2
  B. 1 2 3
  C. 0 3 4
  D. 1 4 2

1. A
2. A
3. A
4. B
5. B *
6. A
7. C
8. B, C
9. C
10. A
11. B
12. C
13. C
14. A
15. C