package lt.myproject.persistence.repository;

import java.sql.Connection;

public abstract class SqlRepository {

        private Connection connection;

        protected SqlRepository(Connection connection) {
            this.connection = connection;
        }

        protected Connection getConnection() {
            return connection;
        }
    }
