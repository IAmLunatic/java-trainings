package lt.myproject.persistence.model;

public class Flat {
    private double flatSizeM2;
    private Integer bedroms;
    private String heatingSytem;
    private String additionPremises;
    private String comments;
    private boolean balcony;

    public Flat(double flatSizeM2, Integer bedroms, String heatingSytem, String additionPremises, String comments, boolean balcony) {
        this.flatSizeM2 = flatSizeM2;
        this.bedroms = bedroms;
        this.heatingSytem = heatingSytem;
        this.additionPremises = additionPremises;
        this.comments = comments;
        this.balcony = balcony;
    }
    public double getFlatSizeM2() {
        return flatSizeM2;
    }

    public Integer getBedroms() {
        return bedroms;
    }

    public String getHeatingSytem() {
        return heatingSytem;
    }

    public String getAdditionPremises() {
        return additionPremises;
    }

    public String getComments() {
        return comments;
    }

    public boolean isBalcony() {
        return balcony;
    }
}

