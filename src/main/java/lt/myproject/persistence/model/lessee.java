package lt.myproject.persistence.model;

public class Lessee {

        private String nickName;
        private String password;
        private String email;
        private Integer telphoneNumber;

    public Lessee(String nickName, String password, String email, Integer telphoneNumber) {
            this.nickName = nickName;
            this.password = password;
            this.email = email;
            this.telphoneNumber = telphoneNumber;
        }
    public String getNickName() {
        return nickName;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    public Integer getTelphoneNumber() {
        return telphoneNumber;
    }
    }

