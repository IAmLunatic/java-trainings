package com.baltictalents.lessons.db.model;

import java.time.LocalDateTime;
import java.util.Optional;

public class Comment {

    private Integer id;
    private String webpage;
    private String summary;
    private String comment;
    private Optional<Integer> userId;
    private LocalDateTime createdOn;

    public Comment(String webpage, String summary, String comment, Optional<Integer> userId, LocalDateTime createdOn) {
        this.webpage = webpage;
        this.summary = summary;
        this.comment = comment;
        this.userId = userId;
        this.createdOn = createdOn;
    }

    public Comment(Integer id, String webpage, String summary, String comment, Optional<Integer> userId, LocalDateTime createdOn) {
        this.id = id;
        this.webpage = webpage;
        this.summary = summary;
        this.comment = comment;
        this.userId = userId;
        this.createdOn = createdOn;
    }

    public Integer getId() {
        return id;
    }

    public String getWebpage() {
        return webpage;
    }

    public String getSummary() {
        return summary;
    }

    public String getComment() {
        return comment;
    }

    public Optional<Integer> getUserId() {
        return userId;
    }

    public LocalDateTime getCreatedOn() {
        return createdOn;
    }
}
