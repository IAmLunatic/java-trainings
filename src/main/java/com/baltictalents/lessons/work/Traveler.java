package com.baltictalents.lessons.work;

public class Traveler implements Comparable<Traveler> {

    private final String name;
    private final Integer age;
    private final Character sex;

    public Traveler(String name, Integer age, Character sex) {
        this.name = name;
        this.age = age;
        this.sex = sex;
    }

    public String getName() {
        return name;
    }

    public Integer getAge() {
        return age;
    }

    public Character getSex() {
        return sex;
    }

    @Override
    public String toString() {
        return "Traveler(" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", sex=" + sex +
                ')';
    }

    @Override
    public int compareTo(Traveler o) {

        if (o.getAge() == age) {
            return o.getName().compareTo(name);
        } else {
            return o.getAge() - age;
        }
    }
}
