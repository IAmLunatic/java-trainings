package com.baltictalents.lessons.work.realestate;

import java.math.BigDecimal;

public class Location {

    private final Coordinates coordinates;
    private final String country;
    private final String city;
    private final String address;
    private final String zipcode;


    public Location(Coordinates coordinates, String country, String city, String address, String zipcode) {
        this.coordinates = coordinates;
        this.country = country;
        this.city = city;
        this.address = address;
        this.zipcode = zipcode;
    }
}
