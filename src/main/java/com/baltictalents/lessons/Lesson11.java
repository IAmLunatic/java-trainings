package com.baltictalents.lessons;

public class Lesson11 {

    //overloading

    void lesson() {
        println("sfdsg");
        println(24325435, 354356);
        println((Object)"wresfddsg");
    }

    public static void println(String s) {
        System.out.println("String: " + s);
    }

    public static void println(Integer i, Integer j) {
        System.out.println("Integer: " + (i + j));
    }

    public static void println(Object object) {
        System.out.println(object);
    }
}
