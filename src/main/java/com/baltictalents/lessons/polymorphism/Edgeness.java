package com.baltictalents.lessons.polymorphism;

public interface Edgeness {

    Integer edgesCount();
}
