package com.baltictalents.lessons.polymorphism;

public class Circle implements Figure {

    private Double radius;

    public Circle(Double radius) {
        this.radius = radius;
    }

    @Override
    public Double area() {
        return Math.PI * radius * radius;
    }

    @Override
    public Double perimeter() {
        return 2 * Math.PI * radius;
    }

    public Double getRadius() {
        return radius;
    }

    @Override
    public String toString() {
        return "Circle(" + radius + ")";
    }
}
