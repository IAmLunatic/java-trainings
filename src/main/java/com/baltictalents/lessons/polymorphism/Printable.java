package com.baltictalents.lessons.polymorphism;

public interface Printable {

    String stringify();
}
