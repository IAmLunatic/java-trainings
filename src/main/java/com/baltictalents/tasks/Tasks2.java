package com.baltictalents.tasks;

import java.math.BigDecimal;
import java.util.Arrays;

public class Tasks2 {

    /*
    Duoti trys skaičiai: a, b, c. Nustatykite ar šie skaičiai gali būti
    trikampio kraštinių ilgiai ir jei gali tai kokio trikampio:
    lygiakraščio, lygiašonio ar įvairiakraščio. Atspausdinkite
    atsakymą. Kaip pradinius duomenis panaudokite tokius skaičius:
    3, 4, 5
    2, 10, 8
    5, 6, 5
    5, 5, 5
     */
    void triangle(){
        Integer[][] triangles = {
                {3, 4, 5},
                {2, 10, 8},
                {5, 6, 5},
                {5, 5, 5}
        };
        for (Integer[] triangleSides : triangles)

        {
            Integer a = triangleSides[0];
            Integer b = triangleSides[1];
            Integer c = triangleSides[2];

            task1(a, b, c);
            task2(a, b, c);

        }

    }
    void task1( Integer a,Integer  b,Integer  c) {
        if (a + b > c && a + c > b && b + c > a) {
            if (a == b && b == c && a == c)
            {
                //return "This triangle has equal edges";
                System.out.println("This triangle has equal edges");
            }
            else if (a == b || a == c || b == c)
            {
                //return "This triangle has  two equal edges";
                System.out.println("This triangle has  two equal edges");
            }
            else
            {
                System.out.println("This is triangle");
                //return b+"This is triangle";
            }
        }
        else
        //return b+"There is no such triangle";
        {
            //return "There is no such triangle";
            System.out.println("There is no such triangle");
        }
    }


    /*
    Apskaiciuoti trikampiu plotus
    3, 4, 5
    2, 10, 8
    5, 6, 5
    5, 5, 5
     */
    void task2(int a, int b, int c) {
        if(a + b > c && a + c > b && b + c > a){
            Integer perimeter = a + b + c;
            Integer p = perimeter / 2;
            BigDecimal heron = new BigDecimal(Math.sqrt(p*(p-a)*(p-b)*(p-c)));
            System.out.println(heron);

        }
    }

    // duotas sveikuju skaiciu masyvas {-5, 4, 10, -4, 0, 7, -8, 9, 8, 15}
    // surasti min ir max
    // negalima naudoti min, max;
    public Integer[] a = {-5, 4, 10, -4, 0, 7, -8, 9, 8, 15};

    public void task3(Integer[] arr) {
        Integer temp;
        for (int i = 0; i < arr.length -1; i++) {
            for (int j = i+1; j < arr.length; j++) {
                if (arr[i] > arr[j]) {
                    temp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;
                }
            }
        }
        System.out.println(Arrays.toString(arr));
    }

    // duotas sveikuju skaiciu masyvas {-5, 4, 10, -4, 0, 7, -8, 9, 8, 15}
    // isrikiuoti didejimo tvarka
    // negalima naudoti Collections.sort(array[Int]);
    void task4(Integer[] arr) {
        Integer Min = a[0];
        Integer Max = a[arr.length-1];

        for (int i = 1; i < arr.length; i++){
            if(Min > a[i]){
                Min = a[i];
            }


        }
        for (int i = arr.length - 2; i <= 0; i--){
            if (Max < a[i]){
                Max = a[i];
            }
        }
        System.out.println("Array Min is: " + Min);
        System.out.println("Array Max is: " + Max);
    }
}
