package com.baltictalents.tasks;

import com.baltictalents.Main;

import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.IntStream;

public class Tasks8 {

    Map<Integer, Integer> list = new HashMap<>();

    //    Sukurkite programą kur keletas objektų (tarkime kokie 5) vienu
//    metu sugeneruoja po šimtą atsitiktinių skaičių iš intervalo
//    1..100 (imtinai). Išveskite tuos skaičius didėjimo tvarka kartu su
//    pasikartojimo skaičiumi, pvz.: 1 (5), 3 (1), ... - tai reiškia, kad
//    skaičius 1 buvo sugeneruotas 5 kartus, skaičius 2 - visai
//    nebuvo, 3 - vieną kartą ir t.t.
    public void  task1() throws Exception {


        ExecutorService executor = Executors.newFixedThreadPool(5);

        Tasks8 main = new Tasks8();

        for (int i = 1; i < 5; i++) {
            executor.execute(() -> {
                IntStream.range(1, 101)
                        .forEach(main::printer);
            });
        }
    }
    List<Integer> squares = new ArrayList<>();
    void printer(Integer i) {

        synchronized (list) {
            if (!list.containsKey(i)) {
            list.put(list.hashCode(), i);
            }
        }
        System.out.println(i);
        //System.out.println(Arrays.asList(list));
        System.out.println(Collections.singletonList(list));
        //System.out.println(Arrays.toString(list.toArray()));
    }
}



