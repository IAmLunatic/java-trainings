package com.baltictalents.tasks;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class Tasks4 {

    public void example() throws Exception{
        PrintWriter U1 = new PrintWriter("U1.txt", "UTF-8");
        int minN = 1;
        int maxN = 30;
        Random random = new Random();
        int randomN = ThreadLocalRandom.current().nextInt(minN, maxN + 1);
        char[] colors = {'G', 'Z', 'R' };

        U1.println(randomN);
        for (int j = 1; j <= randomN; j++){
            char color = colors[random.nextInt(colors.length-1)+1];
            int randomNumb = random.nextInt(99) + 1;
            U1.println(color + " " + randomNumb);
            //continue;
        }
        U1.close();
    }
    public void egzaminas() throws Exception{
        File U1 = new File("U1.txt");
        Scanner sc = new Scanner(U1);
        Integer rowNumb = sc.nextInt();
        int green = 0;
        int yellow = 0;
        int red = 0;
        char[] colors = new char[rowNumb];
        int[] items = new int[rowNumb];
        char z = 'Z';
        char g = 'G';
        char r = 'R';
        for (int i = 0; i < rowNumb; i++){
            colors[i] = sc.next().charAt(0);
           if (colors[i] == z){
               items[i] = sc.nextInt();
               green += items[i];
           }
           if (colors[i] == g){
               items[i] = sc.nextInt();
               yellow += items[i];
           }
           if (colors[i] == r){
               items[i] = sc.nextInt();
               red += items[i];
               }
    }
    sc.close();
        PrintWriter U1rez = new PrintWriter("U1rez.txt", "UTF-8");
        int min = Math.min(green, Math.min(yellow, red));
        U1rez.println(min);
        U1rez.println(g + " = " +(yellow - min));
        U1rez.println(z + " = " +(green - min));
        U1rez.println(r + " = " +(red - min));
        U1rez.close();

            }



    // http://www.egzaminai.lt/failai/7417_IT-VBE-1_2018-GALUTINE.pdf pirma uzduotis
    // kaip ir minejau paskaitos metu, pabandykime isspresti valstybinio egzamino uzduoti

//        public void task1()  throws Exception{
//            PrintWriter writer = new PrintWriter("java-trainings/src/com/baltictalents/resources/input.txt", "UTF-8");
//            writer.println(3);
//            writer.println("Z 10");
//            writer.println("G 3 4 5");
//            writer.println("R 3");
//            writer.close();
//        }
//       public void skaityk() throws Exception{
//            File file = new File ("java-trainings/src/com/baltictalents/resources/input.txt");
//            Scanner sc = new Scanner(file);
//            Integer numbRow = sc.nextInt();
//            char[] names = new char[numbRow];
//            int[] data = new int[numbRow];
//            for (int i = 0; i < numbRow; i++){
//                char c = sc.next().charAt(0);
//                names[i] = c;
//                while(true){
//                    if (sc.hasNextInt() == false){
//                        break;
//                    }
//                    Integer x = sc.nextInt();
//                    data[i] = x;
//                    System.out.println(x);
//                }
//                System.out.println(c);
//            }
//        }



    // antra to pacio valstybinio egzamino uzduotis
    void task2() {


    }
}
