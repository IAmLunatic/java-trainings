package com.baltictalents.tasks;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Tasks3 {


    // uzpildyti sveikuju skaiciu masyva fibonaci seka. 1000 elementu.
    void task1() {
        BigDecimal count = new BigDecimal(1000);
        BigDecimal num1 = new BigDecimal(0);
        BigDecimal num2 = new BigDecimal (1);
        BigDecimal sumOfPrevTwo;
        System.out.println("Fibonacci Series of "+count+" numbers:");
        for (BigDecimal i = BigDecimal.ZERO; i.compareTo(count) < 0; i = i.add(BigDecimal.ONE))
        {
            System.out.println(num1+" ");
            sumOfPrevTwo = num1.add(num2);
            num1 = num2;
            num2 = sumOfPrevTwo;
        }
    }
    // parasyti funkcija kuri nustato ar sveikasis skaicius yra pirminis skaicius.
    public void task2(Integer a) {
        //BigDecimal zero = new BigDecimal(1);
        //BigDecimal one = new BigDecimal(2);
        Integer counter = 1;
        BigDecimal anyNumber = new BigDecimal(a);
        //BigDecimal integerNumb = new BigDecimal(a);
        //integerNumb = integerNumb.setScale(0, RoundingMode.HALF_DOWN);
//        if (anyNumber.compareTo(integerNumb) == 0){
            for (BigDecimal i = BigDecimal.ONE; i.compareTo(anyNumber) < 0; i = i.add(BigDecimal.ONE)){
                BigDecimal remainder = anyNumber.remainder(i);

                if (BigDecimal.ZERO.compareTo(remainder) == 0) {
                    counter++;
                    //System.out.println(counter);
                }
            }
            if (counter == 2){
                System.out.println( anyNumber.toString()+"is initial number" );
            }
            else {
                System.out.println("Not initial number");
            }
//        }
//        else {
//            System.out.println("not integer number");
//        }
    }

    // rasti pirmus 100 pirminiu skaiciu ir uzpildyti jais masyva
    void task3() {
        Integer count = 100;
        Integer b;
        for (int i = 2; i <= count; ++i){
            Integer counter = 0;
            for (int j = 2; j < i; ++j){
                b = i % j;
                if (b == 0){
                    counter++;
                }
            }
            if (counter == 0){
                System.out.println(i);
            }
        }
    }
    // aprasyti 3 skirtingas klases kurios tarpusavyje turetu logiska sarysi
    void task4() {
        class Car{
            String model;
            Integer buildYear;
            String color;
            String gearbox;
        }
        class CarEngine{
            Double liter;
            Boolean turbine;
            String fuealType;
            Integer euroStandart;
        }
        class CarAmortiztion{
            Integer numbOfOwners;
            Boolean damaged;
            Integer mileage;
            String notes;
        }
    }
}
