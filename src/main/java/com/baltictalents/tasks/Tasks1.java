package com.baltictalents.tasks;

import java.util.Scanner;

public class Tasks1 {

    // programa papraso ivesti skaiciu, i ekrana reikia ivesti ar skaicius lyginis ar nelyginis
    void task1() {


        Double a = readDouble("a");
        Double b = readDouble("b");
        Double c = readDouble("c");

    }

    Double readDouble(String paramName) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Input digit " + paramName + " = ");
        Double a = scanner.nextDouble();
        return a;
    }

    // programa papraso ivesti skaiciu, i ekrana isvedama skaiciaus saknis
    void task2() {

        Scanner scanner = new Scanner(System.in);
        System.out.print("Please enter a number b = ");
        Double b = scanner.nextDouble();
        scanner.close();
        System.out.printf("squere root of b is %.4f %.4f", Math.sqrt(b), (-1)*Math.sqrt(b));
    }

    // programa papraso ivesti reiksmes a,b,c, kurios yra kvadratines lygties ax*x+bx+c reiksmes,
    // i ekrana isvedama kvadratines lygties sprendimas x1 = ?, x2 = ?
    void task3() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Please enter first number for squere function ax*x+bx+c, a = ");
        Double a = scanner.nextDouble();
        System.out.print("Please enter second nuber for squere function ax*x+bx+c, b =");
        Double b = scanner.nextDouble();
        System.out.print("Please enter third number for squere function ax*x+bx+c, c =");
        Double c = scanner.nextDouble();
        scanner.close();
        Double result1 = -1*(b/2*a);
        Double result2 = (-1*b + Math.sqrt(b*b - 4*a*c))/2*a;
        Double result3 = (-1*b - Math.sqrt(b*b - 4*a*c))/2*a;

        if (a == 0){
            System.out.println("At quadratic functions value a can't be equal to zero, please choose other value for a ");
        }
        else if(b*b -4*a*c < 0){
            System.out.println("There are no real roots");
        }
        else if(b*b -4*a*c == 0) {
            System.out.println("The root for this quadratic function is :"+result1 );
        }
        else if(b*b -4*a*c > 0) {
            System.out.println("The roots for this quadratic function is : "+result2+" and "+ result3);

        }
    }

    // programa papraso ivesti reiksmes a,b,c, kurios yra kvadratines lygties ax*x+bx+c reiksmes,
    // i ekrana isvedam kvadratines lygties reiksmes intervale [-5, 5]
    void task4() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Please enter first number for squere function ax*x+bx+c, a = ");
        Double a = scanner.nextDouble();
        System.out.print("Please enter second nuber for squere function ax*x+bx+c, b =");
        Double b = scanner.nextDouble();
        System.out.print("Please enter third number for squere function ax*x+bx+c, c =");
        Double c = scanner.nextDouble();
        scanner.close();
        Double result1 = -1*(b/2*a);
        Double result2 = (-1*b + Math.sqrt(b*b - 4*a*c))/2*a;
        Double result3 = (-1*b - Math.sqrt(b*b - 4*a*c))/2*a;

        if (a == 0){
            System.out.println("At quadratic functions value a can't be equal to zero, please choose other value for a ");
        }
        else if(b*b -4*a*c < 0 ){
            System.out.println("There are no real roots");
        }
        else if(b*b -4*a*c == 0 && result1 >= -5 && result1 <= 5 ) {
            System.out.println("The root for this quadratic function is :"+result1 );
        }
        else if(b*b -4*a*c > 0 && result2 >= -5 && result2 <= 5 && result3 >= -5 && result3 <= 5) {
            System.out.println("The roots for this quadratic function is : "+result2+" and "+result3);

        }
        else if(b*b -4*a*c > 0 && result2 >= -5 && result2 <= 5) {
            System.out.println("The roots for this quadratic function is : "+result2);

        }
        else if(b*b -4*a*c > 0 && result3 >= -5 && result3 <= 5) {
            System.out.println("The roots for this quadratic function is : "+result3);

        }
    }

    String oddOrEven(Double a) {
        String result = "";
        if (a % 2 == 0) {
            result = "Even";
        } else {
            result = "Odd";
        }
        return result;
    }

    XTuple solveSquareEquation(Double a, Double b, Double c) {

        Double bRoot = b * b;
        Double d = bRoot - 4 * a * c;
        XTuple result = new XTuple();
        if (d >= 0) {
            Double dRoot = Math.sqrt(d);
            result.x1 = x(a, b, dRoot);
            result.x2 = d == 0 ? null : x(a, b, -dRoot);
        } else {
            result.x1 = null;
            result.x2 = null;
        }
        return result;
    }

    Double x(Double a, Double b, Double dRoot) {
        return (-b + dRoot) / (2 * a);
    }

    class XTuple {
        Double x1;
        Double x2;
    }
}
