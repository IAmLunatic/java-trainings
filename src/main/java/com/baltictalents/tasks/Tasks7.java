package com.baltictalents.tasks;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import jdk.nashorn.internal.parser.JSONParser;
import org.json.simple.JSONObject;
import sun.net.www.protocol.http.HttpURLConnection;

import javax.net.ssl.HttpsURLConnection;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;


import static jdk.nashorn.internal.runtime.PropertyDescriptor.GET;

public class Tasks7 {

    // testuojantis naudoti POSTMAN

    // sukurti http servisa, kuris turetu du endpointus:
    // 1. GET http://localhost:8080/currencies
    // 2. GET http://localhost:8080/currency/rates?currencyCode=USD
    // responsai turi buti grazinami json formatu.
    //  pvz: ["USD", "EUR", "GBP"]
    // valiutu kursus gauti is vieso API
    // kad per daznai nenaudoti vieso API del greitaveikos,
    // valiutu kursus teks laikinai saugoti atmintyje valanda laiko.

    static List<String> list = new ArrayList<>();

    Tasks7 Thread = new Tasks7();

    public static void main(String[] args) throws Exception {

        URL url = new URL(null,"https://api.exchangeratesapi.io/latest", new sun.net.www.protocol.https.Handler());
        HttpsURLConnection conn = (HttpsURLConnection)url.openConnection();
        conn.setRequestMethod("GET");

        conn.connect();

        int responsecode = conn.getResponseCode();

        if(responsecode != 200)
            throw new RuntimeException("HttpResponseCode: " +responsecode);
        else {
            Scanner sc = new Scanner(url.openStream());
            while (sc.hasNext()) {
                list.add(sc.nextLine());
            }
            System.out.println(Arrays.toString(list.toArray()));
            //System.out.println(inline);
            sc.close();
        }

//        for (String inline : list){
//            JSONParser parse = new JSONParser();
//            JSONObject jobj = (JSONObject)parse.parse(inline);
//        }

        HttpServer server = HttpServer.create(new InetSocketAddress(8008), 0);
        server.createContext("/currencies", new MyHandler());
        server.createContext("/currency/rates", new MyHandler());
        server.setExecutor(null); // creates a default executor
        server.start();

    }

    // http routes handler
    static class MyHandler implements HttpHandler {
        @Override
        public void handle(HttpExchange t) throws IOException {
            //String response = "[\"USD\", \"EUR\", \"GBP\"]";
            String response = Arrays.toString(list.toArray());
            t.sendResponseHeaders(200, response.length());
            OutputStream os = t.getResponseBody();
            os.write(response.getBytes());
            os.close();
        }
    }
    static class CurrencyRateHandler implements HttpHandler {
        @Override
        public void handle(HttpExchange t) throws IOException {
            String response = "[\"USD\", \"EUR\", \"GBP\"]";
            t.sendResponseHeaders(200, response.length());
            OutputStream os = t.getResponseBody();
            os.write(response.getBytes());
            os.close();
        }
    }
}
