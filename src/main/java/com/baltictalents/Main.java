package com.baltictalents;

import com.baltictalents.lessons.db.MysqlConnector;
import com.baltictalents.lessons.db.model.User;
import com.baltictalents.lessons.db.model.UserCommentsCount;
import com.baltictalents.lessons.db.repository.CommentsRepository;
import com.baltictalents.lessons.db.repository.UsersRepository;
import com.baltictalents.lessons.service.UserService;


public class Main {

    public static void main(String[] args) throws Exception{
    }

    static void useService() throws Exception {
        MysqlConnector mysqlConnector = new MysqlConnector();

        UsersRepository usersRepository =
                new UsersRepository(mysqlConnector.getConnection());

        CommentsRepository commentsRepository =
                new CommentsRepository(mysqlConnector.getConnection());


        UserService userService = new UserService(usersRepository, commentsRepository);

        User user = new User(8, "u11", "borat@email");

        userService.saveOrUpdate(user);

        userService.getUserCommentsCount().forEach(System.out::println);
    }

    static void useRepositories() throws Exception {
        MysqlConnector mysqlConnector = new MysqlConnector();

        CommentsRepository commentsRepository =
                new CommentsRepository(mysqlConnector.getConnection());

        UsersRepository usersRepository =
                new UsersRepository(mysqlConnector.getConnection());

        usersRepository.save(new User(null, "adomas1", "aadomas@gmail.com"));
        usersRepository.save(new User(null, "adomas1", "adomas@gmail.com"));
    }
}
