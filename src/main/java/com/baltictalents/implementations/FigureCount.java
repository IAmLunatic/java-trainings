package com.baltictalents.implementations;

public class FigureCount {
    public int type;
    public int count;

    public FigureCount(int type, int count) {
        this.type = type;
        this.count = count;
    }
}
