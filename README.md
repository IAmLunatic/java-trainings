Trumpa git instrukcija:

Susikonfiguruoti git'a, kad galėtumėt sinchronizuoti su forkint'1 repozitorija:
 a. git remote add upstream git@bitbucket.org:adambit/java-trainings.git
 b. git fetch upstream
 c. git pull upstream master

a, b ir b komandas reikės iškviest tik vieną kartą. a - komanda pridės mano repozitorijos adresą pas jus į git'ą, b - greičiausiai nieko nepadarys, nes Jūs jau esate nusiklonavę visa repozitoriją, bet nieko nesugadins.

c komandą reikės kviesti prieš kiekvieną paskaitą, kad gautumėt atnaujintą medžiagą.

Susikonfiguruoti git'ą, kad nebeprašytų slaptažodžio kiekvieną kartą darant push. Čia yra geras pavyzdys kaip tą padaryti:

https://confluence.atlassian.com/bitbucket/set-up-an-ssh-key-728138079.html

Supushinti pakeitimus:

Jeigu sukurem nauja faila:

git add {failo_kelias}

git commit -am "žinutė" - žinutė pasistenkit kuo aiškesnę, pavyzdžiui "Klasė zmogus atributai".
git push origin master - po šitos komandos viskas atsiduria bitbucket remote aplinkoj ir jau galiu aš pažiūrėti.

Beja komandos git status ir git diff nėra būtinos jos neatlieka jokio veiksmo, bet yra naudingos norint pasitikrinti kas vyksta projekte.
git status - parodo ištrintus, pakeistus arba naujus failus
git diff - parodo kokias eilutes kode pakeitėt.
